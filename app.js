const express = require('express')
const mongoose = require('mongoose')
const app = express()
const morgan = require('morgan')
const cors = require('cors')

const PORT = 3000

require('dotenv').config()

const authApis = require('./routes/AuthAPI')
const userApis = require('./routes/UserAPI')

// Handling Content-Type: application/x-www-form-urlencoded
app.use(express.urlencoded({
	extended: true
}))

//JSON Middleware
app.use(express.json())

//Cors Middleware
app.use(cors())

//Morgan Middleware
app.use(morgan('combined'))

//Connecting to the Database
mongoose.connect(process.env.MONGODB_URI_PUB, {
    useNewUrlParser: true, 
	useCreateIndex: true,
	useUnifiedTopology: true,
	useFindAndModify: false
}).then( () => {
    
    console.log('Whose Calling Db Connected Successfully')
    
}).catch( (error) => {

    console.log('Whose Calling Database Error : ' + error.message)

})

//Adding Auth APIS
app.use('/api/auth/', authApis)

//Adding User CRUD APIS
app.use('/api/user/', userApis)

//Index Page
app.get('/', (req, res) => {

	res.status(200).json({ message: "Welcome to Whose Calling APIS"} )

})


//Start Listening on the App
app.listen(PORT, () => {
	console.log(`Listening on : http://localhost:${PORT}`)
})