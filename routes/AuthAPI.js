const { Router } = require('express')
const User = require("../models/UserModel")
const loginLog = require("../models/LoginLogModel")
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs');
const router  = Router()
const multer  = require('multer')
const upload = multer()

//User Logout Route
router.put('/logout', upload.none(), async (req, res) => {

    var { email } = req.body

    try {

        //Find User and Update
        await User.findOneAndUpdate({ email: email }, { authToken: '' }, () => {
            return res.status(200).json({
                status: "SUCCESS"
            })
        })

    }catch (error) {
        res.status(500).json({
            status: "FAILURE",
            message: error.message
        })
    }
})

//Account Validation on Login
router.post('/login', upload.none(), async (req, res) => {

	var { email, password } = req.body

	try {

		await User.find( {email : email}, (error, userData) => {

			if (userData.length == 1) {

				//Verify Password
				if (bcrypt.compareSync(password, userData['0']['password'])) {

					//Store Login Log
					const newLoginLog = new loginLog({
						userId: userData['0']['_id'],
						browser: '-',
						os: '-',
						ip: '-'
					}).save().then( data => {
						//Generate Auth Token
						const authToken = jwt.sign({ user: userData['0'] }, process.env.ENCRYPTION_KEY);

						//Update Token to User Data
						User.findByIdAndUpdate(userData['0']['_id'], { authToken: authToken })

						//Return Response
						res.status(200).json({ 
							status: "SUCCESS", 
							authToken: authToken, 
							userId: userData['0']['_id']
						})
												
					}).catch( error => {
						res.status(500).json({ 
							status: "FAILURE",
							message: error.message 
						});
					});

				}else {
					res.status(200).json({ status: false, message: 'Please Enter Correct Email and Password' })
				}

			}else {
				//Email Does not Exists
				res.status(200).json( { status: false, message: 'Email does not Exist' })
	
			}
	
		})

	}catch (error) {
		res.status(500).json( { status: false, message: error.message } )
	}


});

module.exports = router