const { Router } = require('express')
const bcrypt = require('bcryptjs')
const User = require('../models/UserModel')
const jwt = require('jsonwebtoken')
const router = Router()
const multer  = require('multer')
const upload = multer()

//Update User by Id
router.put('/update/:userId', async (req, res) => {

    const { userId } = req.params
    const { firstName, lastName, phoneNumber, email, password } = req.body

    try {

        data = { 
                'firstName' : firstName,  
                'lastName' : lastName, 
                'phoneNumber' : phoneNumber,
                'email' : email
            }

        if (password !== "") {
            const salt = bcrypt.genSaltSync(10);
            const hashedPassword = bcrypt.hashSync(password, salt);

            data.password = hashedPassword
        }

        //Updating User
        const user = await User.findByIdAndUpdate(userId, data, {new: true})

        //Authenticate User
        res.status(200).json({ 
            status: 'SUCCESS' 
        })

    }catch (error) {
        res.status(500).json({ 
            status: 'FAILURE', 
            message: error.message 
        })
    }

})

//Delete User by Id
router.delete('/delete/:userId', async (req, res) => {

    const { userId } = req.params

    try {

        const delUser = await User.findByIdAndRemove(userId)

        res.status(200).json({ 
            status: 'SUCCESS',
            data: delUser 
        })

    }catch (error) {
        res.status(500).json({ 
            status: 'FAILURE', 
            message: error.message 
        })
    }


})

//Get All Users
router.get('/all/', async (req, res) => {

    try {

        const users = await User.find()

        res.status(200).json({ 
            status: 'SUCCESS',
            data: users
        })

    }catch (error) {
        res.status(500).json({ 
            status: 'FAILURE', 
            message: error.message 
        })

    }

})

//Create New User API
router.post('/create/', upload.none(), async (req, res) => {

    const { firstName, lastName, email, password } = req.body

    console.log(req.body)

    try {

        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(password, salt);

        const newUser = await new User({
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: hashedPassword
        })
        .save().then( data => {

            console.log("User ID : " + data['id'])

            //Generate Auth Token
            const authToken = jwt.sign({ user: data['id'] }, process.env.ENCRYPTION_KEY);

            //Update Token to User Data
            User.findByIdAndUpdate(data['id'], { authToken: authToken })
            
            res.status(201).json({
                    status: 'SUCCESS', 
                    authToken: authToken, 
                    firstName: data['firstName'],
                    lastName: data['lastName'],
                    email: data['email']
                })

        }).catch( error => {
            res.status(500).json({ 
                status: 'FAILURE', 
                message: error.message 
            })

        })

    }catch (error) {
        res.status(500).json({ 
            status: 'FAILURE', 
            message: error.message 
        })
    }

})

module.exports = router