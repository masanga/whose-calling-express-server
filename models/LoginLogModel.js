const mongoose = require('mongoose')
const { Schema } = mongoose

const LoginLogSchema = new Schema({

    userId: {
        type: String,
        required: true
    },

    browser: {
        type: String
    },

    os: {
        type: String,
    },

    ip: {
        type: String
    },

    created_at: {
        type: Date,
        default: Date.now
    },

    updated_at: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model('LoginLog', LoginLogSchema)