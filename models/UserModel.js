const mongoose = require('mongoose')
const { Schema } = mongoose

const UserSchema = new Schema({

    firstName: {
        type: String,
        required: [true, 'Firstname is Required']
    },

    lastName: {
        type: String,
        required: [true, 'Lastname is Required']
    },

    email: {
        type: String,
        unique: true,
        required: [true, 'Email is Required'],
        regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },

    password: {
        type: String,
        required: [true, 'Password is Required']
    },

    level: {
        type: Number,
        default: 3
    },

    authToken: {
        type: String
    },

    createdAt: {
        type: Date,
        default: Date.now()
    },

    updatedAt: {
        type: Date,
        default: Date.now()
    }

})

module.exports = mongoose.model('User', UserSchema)