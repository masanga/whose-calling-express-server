const mongoose = require('mongoose')
const { Schema } = mongoose

const phoneNumberIdSchema = new Schema({
    
    firstName: {
        type: String
    },

    lastName: {
        type: String
    },

    phoneCountryCode: {
        type: String,
    },

    phoneNumber: {
        type: String
    },

    isSpam: {
        type: String
    },

    blockedTimes: {
        type: String
    },
    
    createdAt: {
        type: Date,
        default: Date.now()
    },

    updatedAt: {
        type: Date,
        default: Date.now()
    }


})

module.exports = mongoose.model('PhoneNumberId', phoneNumberIdSchema);